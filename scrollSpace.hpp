/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#ifndef FINALPROJECT_SCROLLSPACE_HPP
#define FINALPROJECT_SCROLLSPACE_HPP


#include "Space.hpp"

class scrollSpace : public Space {
public:
    //default constructor
    scrollSpace() = default;
};


#endif //FINALPROJECT_SCROLLSPACE_HPP
