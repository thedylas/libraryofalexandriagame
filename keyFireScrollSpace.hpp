/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#ifndef FINALPROJECT_KEYFIRESCROLLSPACE_HPP
#define FINALPROJECT_KEYFIRESCROLLSPACE_HPP


#include "Space.hpp"

class keyFireScrollSpace : public Space {
public:
    //default constructor
    keyFireScrollSpace() = default;

    //deals damage to the user
    int fireDamage() override;
};


#endif //FINALPROJECT_KEYFIRESCROLLSPACE_HPP
