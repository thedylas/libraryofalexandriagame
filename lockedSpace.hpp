/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#ifndef FINALPROJECT_LOCKEDSPACE_HPP
#define FINALPROJECT_LOCKEDSPACE_HPP

#include "Space.hpp"

class lockedSpace : public Space {
public:
    //default constructor
    lockedSpace() = default;

    //allows the user to unlock the door
    void spaceInteraction() override;
};

#endif //FINALPROJECT_LOCKEDSPACE_HPP
