/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#include <iostream>
#include <limits>
#include <string>
#include "menu.hpp"
#include "Space.hpp"
#include "game.hpp"

//the main menu is used the first time through the game
void menu::useMainMenu() {
    isGoodInput_ = false;
    while (!isGoodInput_) {
        std::cout << "\n*******************************************************\n"
                  << "* Welcome to \033[1;31mThe Burning of The Library of Alexandria\033[0m *\n"
                  << "*******************************************************\n"
                  << "\n           ******************************\n"
                  << "           * What would you like to do? *\n"
                  << "           * 1. Play the game           *\n"
                  << "           * 2. Exit                    *\n"
                  << "           ******************************\n";

        std::cin >> userInputInt_;

        //input validation
        if (userInputInt_ < 1 || userInputInt_ > 2 || std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << std::endl;
            isGoodInput_ = false;
            std::cout << "Please enter a valid input\n";
        }
        if (userInputInt_ == 1 || userInputInt_ == 2) {
            isGoodInput_ = true;
        }
    }
    switch (userInputInt_) {
        case 1: {
            //creates a game object to create and play
            game game1;
            game1.createGame();
            game1.playGame();

            //the secondary menu is called once the game has already been played
            useSecondaryMenu();
            return;
        }

        case 2: {
            std::cout << "Quitting program...\n";
            return;
        }

        default: {
            return;
        }
    }
}

//if the user indicates that they want to play again, the main menu is called, otherwise it quits the game
void menu::useSecondaryMenu() {
    std::cout << "\n*********************************\n"
              << "* Would you like to play again? *\n"
              << "* 1. Yes                        *\n"
              << "* 2. No                         *\n"
              << "*********************************\n";


    std::cin >> userInputInt_;

    //input validation
    if (userInputInt_ < 1 || userInputInt_ > 2 || std::cin.fail()) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << std::endl;
        isGoodInput_ = false;
        std::cout << "Please enter a valid input\n";
    }
    if (userInputInt_ == 1 || userInputInt_ == 2) {
        isGoodInput_ = true;
    }


    switch (userInputInt_) {
        case 1: {
            //creates game object to create and play the game
            game game1;
            game1.createGame();
            game1.playGame();

            useSecondaryMenu();

            return;
        }

        case 2: {
            std::cout << "Quitting program...\n";
            return;
        }

        default: {
            return;
        }
    }
}