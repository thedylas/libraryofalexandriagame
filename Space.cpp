/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#include "Space.hpp"

Space::~Space() = default;


//allows the user to interact with the space they are in
void Space::spaceInteraction() {

}

//deals damage to the user
int Space::fireDamage() {
    return 1;
}

//the building collapse can cause damage to the user
fireDamageEnum Space::buildingCollapse() {
    return NONE;
}


//get functions
Space *Space::getNorth() {
    return north_;
}


Space *Space::getEast() {
    return east_;
}


Space *Space::getSouth() {
    return south_;
}


Space *Space::getWest() {
    return west_;
}

std::string Space::getSpaceName() {
    return spaceName_;
}


spaceEnum Space::getSpaceEnum() {
    return spaceEnumName_;
}


//set functions
void Space::setNorth(Space *north) {
    north_ = north;
}


void Space::setEast(Space *east) {
    east_ = east;
}


void Space::setSouth(Space *south) {
    south_ = south;
}


void Space::setWest(Space *west) {
    west_ = west;
}


void Space::setSpaceName(std::string spaceName) {
    spaceName_ = spaceName;
}


void Space::setIsDiscovered(bool isDiscovered) {
}


void Space::setSpaceEnum(spaceEnum nameEnum) {
    spaceEnumName_ = nameEnum;
}


void Space::setEscapeLocked(bool lockStatus) {
}
