/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#include "lockedSpace.hpp"

//allows the user to unlock the door
void lockedSpace::spaceInteraction() {
    setEscapeLocked(false);
}