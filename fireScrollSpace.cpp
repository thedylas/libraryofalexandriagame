/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#include <limits>
#include "fireScrollSpace.hpp"

//adds damage to the user for being in a space that is on fire
int fireScrollSpace::fireDamage() {
    return 1;
}

//random event happens in this space that can cause damage or decrease in number of steps
fireDamageEnum fireScrollSpace::buildingCollapse() {
    srand(time(nullptr));

    int userChoiceInt = 0;
    bool isGoodInput;

    int chance = rand() % 100 + 1;
    isGoodInput = false;

    //50% chance that the user needs to make a decision in this space type
    if (chance > 50) {
        while (!isGoodInput) {
            std::cout << "A beam collapses onto the ground.\n"
                      << "Would you prefer to try to\n"
                      << "1. Climb over it\n"
                      << "2. Crawl under it\n";
            std::cin >> userChoiceInt;
            if (userChoiceInt < 1 || userChoiceInt > 2 || std::cin.fail()) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << std::endl;
                isGoodInput = false;
                std::cout << "Please enter a valid input\n";
            }
            if (userChoiceInt == 1 || userChoiceInt == 2) {
                isGoodInput = true;
            }
        }

        switch (userChoiceInt) {
            case 1: {
                chance = rand() % 100 + 1;

                //50% chance that the user will take damage
                if (chance > 50) {
                    std::cout
                            << "There was a very high level of smoke in the air, and you fall to the ground momentarily.\n"
                            << "You can now go to one fewer room.\n";
                    return SMOKE;
                }
            }
            case 2: {
                chance = rand() % 100 + 1;

                //50% chance that the user will take damage
                if (chance > 50) {
                    std::cout
                            << "The beam shifts and rubble falls on you as you crawl under the beam.\n"
                            << "You have lost some health.\n";
                    return HEALTH;
                }
            }

            default: {
                return NONE;
            }
        }

    } else { return NONE; }
}
