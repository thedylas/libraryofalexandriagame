/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#include <iostream>
#include "menu.hpp"

int main() {
    //creates a menu object
    menu menu1;

    //runs the main menu
    menu1.useMainMenu();

    return 0;
}