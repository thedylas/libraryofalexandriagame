/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#include <limits>
#include "game.hpp"
#include "Space.hpp"
#include "neutralSpace.hpp"
#include "fireScrollSpace.hpp"
#include "keyFireScrollSpace.hpp"
#include "scrollSpace.hpp"
#include "lockedSpace.hpp"

//this function creates all of the spaces and defines which spaces they point to
void game::createGame() {
    //start
    startSpace_->setSouth(wallSpace_);
    startSpace_->setNorth(entranceSpace_);
    startSpace_->setEast(wallSpace_);
    startSpace_->setWest(wallSpace_);
    startSpace_->setSpaceName("Start");
    startSpace_->setSpaceEnum(START);
    currentSpace_ = startSpace_;
    startSpace_->setIsDiscovered(true);


    //entrance
    entranceSpace_->setSouth(startSpace_);
    entranceSpace_->setNorth(stairsSpace_);
    entranceSpace_->setEast(wallSpace_);
    entranceSpace_->setWest(wallSpace_);
    entranceSpace_->setSpaceName("Entrance");
    entranceSpace_->setSpaceEnum(ENTRANCE);
    entranceSpace_->setIsDiscovered(true);

    //stairs
    stairsSpace_->setSouth(entranceSpace_);
    stairsSpace_->setNorth(southMainLibrarySpace_);
    stairsSpace_->setEast(eastGardenSpace_);
    stairsSpace_->setWest(westGardenSpace_);
    stairsSpace_->setSpaceName("Stairs");
    stairsSpace_->setSpaceEnum(STAIRS);

    //west garden
    westGardenSpace_->setSouth(wallSpace_);
    westGardenSpace_->setNorth(southWestHallSpace_);
    westGardenSpace_->setEast(stairsSpace_);
    westGardenSpace_->setWest(wallSpace_);
    westGardenSpace_->setSpaceName("West Garden");
    westGardenSpace_->setSpaceEnum(WESTGARDEN);

    //east garden
    eastGardenSpace_->setSouth(wallSpace_);
    eastGardenSpace_->setNorth(southEastHallSpace_);
    eastGardenSpace_->setEast(wallSpace_);
    eastGardenSpace_->setWest(stairsSpace_);
    eastGardenSpace_->setSpaceName("East Garden");
    eastGardenSpace_->setSpaceEnum(EASTGARDEN);

    //south main library
    southMainLibrarySpace_->setSouth(stairsSpace_);
    southMainLibrarySpace_->setNorth(libraryMainHallSpace_);
    southMainLibrarySpace_->setEast(southEastHallSpace_);
    southMainLibrarySpace_->setWest(southWestHallSpace_);
    southMainLibrarySpace_->setSpaceName("South Main Library");
    southMainLibrarySpace_->setSpaceEnum(SOUTHMAINLIBRARY);

    //south west hallway
    southWestHallSpace_->setSouth(westGardenSpace_);
    southWestHallSpace_->setNorth(wallSpace_);
    southWestHallSpace_->setEast(southMainLibrarySpace_);
    southWestHallSpace_->setWest(wallSpace_);
    southWestHallSpace_->setSpaceName("South West Hallway");
    southWestHallSpace_->setSpaceEnum(SOUTHWESTHALLWAY);

    //south east hallway
    southEastHallSpace_->setSouth(eastGardenSpace_);
    southEastHallSpace_->setNorth(wallSpace_);
    southEastHallSpace_->setEast(wallSpace_);
    southEastHallSpace_->setWest(southMainLibrarySpace_);
    southEastHallSpace_->setSpaceName("South East Hallway");
    southEastHallSpace_->setSpaceEnum(SOUTHEASTHALLWAY);

    //library east hall
    libraryEastHallSpace_->setSouth(wallSpace_);
    libraryEastHallSpace_->setNorth(northEastHallSpace_);
    libraryEastHallSpace_->setEast(wallSpace_);
    libraryEastHallSpace_->setWest(libraryMainHallSpace_);
    libraryEastHallSpace_->setSpaceName("Library East Hall");
    libraryEastHallSpace_->setSpaceEnum(LIBRARYEASTHALL);

    //library main hall
    libraryMainHallSpace_->setSouth(southMainLibrarySpace_);
    libraryMainHallSpace_->setNorth(northMainLibrarySpace_);
    libraryMainHallSpace_->setEast(libraryEastHallSpace_);
    libraryMainHallSpace_->setWest(libraryWestHallSpace_);
    libraryMainHallSpace_->setSpaceName("Library Main Hall");
    libraryMainHallSpace_->setSpaceEnum(LIBRARYMAINHALL);

    //library west hall
    libraryWestHallSpace_->setSouth(wallSpace_);
    libraryWestHallSpace_->setNorth(northWestHallSpace_);
    libraryWestHallSpace_->setEast(libraryMainHallSpace_);
    libraryWestHallSpace_->setWest(wallSpace_);
    libraryWestHallSpace_->setSpaceName("Library West Hall");
    libraryWestHallSpace_->setSpaceEnum(LIBRARYWESTHALL);

    //north west hall
    northWestHallSpace_->setSouth(libraryWestHallSpace_);
    northWestHallSpace_->setNorth(wallSpace_);
    northWestHallSpace_->setEast(northMainLibrarySpace_);
    northWestHallSpace_->setWest(wallSpace_);
    northWestHallSpace_->setSpaceName("North West Hall");
    northWestHallSpace_->setSpaceEnum(NORTHWESTHALL);

    //north main library
    northMainLibrarySpace_->setSouth(libraryMainHallSpace_);
    northMainLibrarySpace_->setNorth(escapeSpace_);
    northMainLibrarySpace_->setEast(northEastHallSpace_);
    northMainLibrarySpace_->setWest(northWestHallSpace_);
    northMainLibrarySpace_->setSpaceName("North Main Library");
    northMainLibrarySpace_->setSpaceEnum(NORTHMAINLIBRARY);

    //north east hall
    northEastHallSpace_->setSouth(libraryEastHallSpace_);
    northEastHallSpace_->setNorth(wallSpace_);
    northEastHallSpace_->setEast(wallSpace_);
    northEastHallSpace_->setWest(northMainLibrarySpace_);
    northEastHallSpace_->setSpaceName("North East Hall");
    northEastHallSpace_->setSpaceEnum(NORTHEASTHALL);

    //escape
    escapeSpace_->setSouth(northMainLibrarySpace_);
    escapeSpace_->setNorth(wallSpace_);
    escapeSpace_->setEast(wallSpace_);
    escapeSpace_->setWest(wallSpace_);
    escapeSpace_->setSpaceName("Escape");
    escapeSpace_->setSpaceEnum(ESCAPE);
}

//this function runs the game and calls the turn function
void game::playGame() {
    //while the user is alive and has not escaped then they take a turn
    while (health_ > 0 && steps_ <= 12 && !isEscaped_) {
        turn();
        std::cout << "\033[1;31mRemaining health before the flames consume your body: " << health_ << "\033[0m\n";
        std::cout << "\033[1;1mRemaining number of rooms that can be explored before you die of smoke inhalation: "
                  << 13 - steps_ << "\033[0m\n\n";
    }

    //lose conditions
    if (health_ <= 0) {
        std::cout
                << "\nYou begin to scream as the flames engulf you. You try to pat them out unsuccessfully, and eventually fall to the ground as you burn to death.\n";
    }
    if (steps_ > 12) {
        std::cout
                << "\nYou cannot stop coughing and are short of breath. Your head is pounding as you bend over to throw up.\nYou try to yell out for help but cannot. Slowly you crumple to the ground and close your eyes.\n";
    }
    if (health_ <= 0 || steps_ > 12) {
        std::cout << "\nGood luck next time.\n";
    }

    //win condition
    if (isEscaped_ && health_ > 0 && steps_ <= 12) {
        std::cout << "Congratulations! You have saved the sacred texts!";
        return;
    }
}


void game::turn() {
    //if the user has not escaped yet then they take their turn
    if (!isEscaped_) {
        //prints information about the area
        narrative();
        //prints the map to orient the user
        printMap();
        //allows the user to choose where to go
        directionDecision();
        //increments the step variable to keep track
        steps_++;
    }
}

//this function prints out information about where the user is and what is going on depending on the space
void game::narrative() {
    if (currentSpace_->getSpaceEnum() == START) {
        std::cout << "You approach the great Library of Alexandria.\n"
                  << "It is engulfed in flames! You must run in to save the sacred texts!\n"
                  << "Unfortunately your bag is only big enough to hold 8 items...\n"
                  << "Collect at least 5 scrolls before the library burns to the ground!\n";
    }

    if (currentSpace_->getSpaceEnum() == ENTRANCE) {
        std::cout << "As you approach the entrance of the library it begins to get hotter.\n"
                  << "You see the stairway ahead of you into the fire.\n";
    }

    if (currentSpace_->getSpaceEnum() == STAIRS) {
        std::cout
                << "On the left and right of you are beautiful gardens, but directly ahead of you lies the burning Library\n"
                << "You see some scrolls on the stairs. One of them is one of the ancient texts! Someone must have dropped it as they fled the library!\n";
        itemPickup("Scroll");
    }

    if (currentSpace_->getSpaceEnum() == WESTGARDEN) {
        std::cout << "The beautiful garden is untouched so far by the fire.\n"
                  << "Ahead of you is a hall that leads to the library. The fire seems to have reached it already.\n";
    }

    if (currentSpace_->getSpaceEnum() == EASTGARDEN) {
        std::cout << "The beautiful garden is untouched so far by the fire.\n"
                  << "Ahead of you is a hall that leads to the library. The fire seems to have reached it already.\n";
    }

    if (currentSpace_->getSpaceEnum() == SOUTHEASTHALLWAY) {
        std::cout << "You breath a sigh of relief, this hallway has not yet been touched by the flames.\n"
                  << "To the West lies the main library, there are likely some more scrolls there.\n"
                  << "One of the ancient texts is in this hallway!\n";
        itemPickup("Scroll");
    }

    if (currentSpace_->getSpaceEnum() == SOUTHMAINLIBRARY) {
        health_ -= southMainLibrarySpace_->fireDamage();
        std::cout << "As you enter the burning section of the main library you feel weak\n"
                  << "North lies the main hall, where most of the sacred texts were kept.\n"
                  << "One of the ancient texts is here!\n";
        itemPickup("Scroll");
        buildingCollapse();
    }

    if (currentSpace_->getSpaceEnum() == SOUTHWESTHALLWAY) {
        health_ -= southWestHallSpace_->fireDamage();
        std::cout << "You feel weaker as you step into this burning hallway.\n"
                  << "To the East lies a main library where many scrolls are kept.\n"
                  << "One of the ancient texts is in this hall!\n";
        itemPickup("Scroll");
        buildingCollapse();
    }

    if (currentSpace_->getSpaceEnum() == LIBRARYWESTHALL) {
        health_ -= southWestHallSpace_->fireDamage();
        std::cout << "You feel weaker as you step into this burning hallway. \n"
                  << "To the North lies a hall that leads to the exit!\n"
                  << "One of the ancient texts is in this hallway!\n";
        itemPickup("Scroll");
        std::cout << "You see a key! That might be useful...\n";
        itemPickup("Key");
    }

    if (currentSpace_->getSpaceEnum() == LIBRARYMAINHALL) {
        health_ -= libraryMainHallSpace_->fireDamage();
        std::cout << "You feel weaker as you step into this burning hallway. \n"
                  << "To the East and West lie some large halls, they may contain important documents...\n"
                  << "One of the ancient texts is in this hallway!\n";
        itemPickup("Scroll");
        buildingCollapse();
    }

    if (currentSpace_->getSpaceEnum() == LIBRARYEASTHALL) {
        health_ -= libraryEastHallSpace_->fireDamage();
        std::cout << "You feel weaker as you step into this burning hallway. \n"
                  << "To the North lies a hall that leads to the exit!\n"
                  << "One of the ancient texts is in this hallway!\n";
        itemPickup("Scroll");
        std::cout << "You see a key! That might be useful...\n";
        itemPickup("Key");
    }

    if (currentSpace_->getSpaceEnum() == NORTHEASTHALL) {
        health_ -= northEastHallSpace_->fireDamage();
        std::cout << "You feel weaker as you step into this burning hallway. \n"
                  << "To the West lies the main library, it is close to the way out!\n"
                  << "One of the ancient texts is in this hallway!\n";
        itemPickup("Scroll");
        buildingCollapse();
    }

    if (currentSpace_->getSpaceEnum() == NORTHMAINLIBRARY) {
        health_ -= northMainLibrarySpace_->fireDamage();
        std::cout << "As you enter the main section of the library you immediately see one of the scrolls.\n";
        itemPickup("Scroll");
        buildingCollapse();
        std::cout << "Ahead lies a door to escape the burning library, but it looks like it's locked\n";
    }

    if (currentSpace_->getSpaceEnum() == NORTHWESTHALL) {
        health_ -= northWestHallSpace_->fireDamage();
        std::cout << "You feel weaker as you step into this burning hallway. \n"
                  << "To the East lies the main library, it is close to the way out!\n"
                  << "One of the ancient texts is in this hallway!\n";
        itemPickup("Scroll");
        buildingCollapse();
    }

    if (currentSpace_->getSpaceEnum() == ESCAPE) {
        std::cout << "Congratulations! You successfully escaped the library with all of the sacred texts!\n"
                  << "As you look behind you, you see the exit you just came out of collapse...\n";
        isEscaped_ = true;
    }
}


//allows the user to orient themselves
void game::printMap() {
    //north space
    std::cout << "****************************************************************************\n"
              << "                              " << currentSpace_->getNorth()->getSpaceName() << "\n"
              << "                            " << "     .\n"
              << "                            " << "   .:;:.        \n"
              << "                            " << " .:;;;;;:.        \n"
              << "                            " << "   ;;;;;      \n"
              << "                            " << "   ;;;;;       \n"
              << "                            " << "   ;;;;;        \n"
              << "                            " << "   ;;;;;\n"
              << "                            " << "   ;:;;;\n"
              << "                            " << "   : ;;;  \n"
              << "                            " << "     ;:;  \n"
              << "                            " << "   . :.;  \n"
              << "                            " << "     . :  \n"
              << "                            " << "   .   .  \n"
              << "                            " << "\n";

    //west, east, and current spaces
    std::cout << "\n                               " << currentSpace_->getSpaceName() << "\n"
              << "    .                " << "                            " << "                    .\n"
              << "  .;;............ .. " << "                            " << "     .. ............;;.\n"
              << ".;;;;::::::::::::..  " << "                            " << "      ..::::::::::::;;;;.\n"
              << " ':;;:::::::::::: . ." << "                            " << "    . . ::::::::::::;;:'\n"
              << "   ':                " << "                            " << "                    :'\n";
    std::cout << currentSpace_->getWest()->getSpaceName() << "                                                        "
              << currentSpace_->getEast()->getSpaceName() << "\n";

    //south space
    std::cout << "                            " << "       .\n"
              << "                            " << "   . ;.\n"
              << "                            " << "    .;\n"
              << "                            " << "     ;;.    \n"
              << "                            " << "   ;.;;     \n"
              << "                            " << "   ;;;;.    \n"
              << "                            " << "   ;;;;;    \n"
              << "                            " << "   ;;;;;   \n"
              << "                            " << "   ;;;;;\n"
              << "                            " << "   ;;;;;\n"
              << "                            " << "   ;;;;;   \n"
              << "                            " << " ..;;;;;..  \n"
              << "                            " << "  ':::::'   \n"
              << "                            " << "    ':`    \n"
              << "                              " << currentSpace_->getSouth()->getSpaceName() << "\n";
}


//allows the user to choose where they want to go
void game::directionDecision() {
    std::string northOption;
    std::string eastOption;
    std::string southOption;
    std::string westOption;

    //creates the menu options
    if (currentSpace_->getNorth() != wallSpace_) {
        northOption = "1. " + currentSpace_->getNorth()->getSpaceName() + "\n";
    } else { northOption = "You can't go North\n"; }
    if (currentSpace_->getEast() != wallSpace_) {
        eastOption = "2. " + currentSpace_->getEast()->getSpaceName() + "\n";
    } else { eastOption = "You can't go East\n"; }
    if (currentSpace_->getSouth() != wallSpace_) {
        southOption = "3. " + currentSpace_->getSouth()->getSpaceName() + "\n";
    } else { southOption = "You can't go South\n"; }
    if (currentSpace_->getWest() != wallSpace_) {
        westOption = "4. " + currentSpace_->getWest()->getSpaceName() + "\n";
    } else { westOption = "You can't go West\n"; }

    isGoodInput_ = false;
    while (!isGoodInput_) {
        std::cout << "****************************************************************************\n"
                  << "Where would you like to go?\n"
                  << northOption
                  << eastOption
                  << southOption
                  << westOption;
        std::cin >> userInputInt_;

        if (userInputInt_ < 1 || userInputInt_ > 4 || std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << std::endl;
            isGoodInput_ = false;
            std::cout << "Please enter a valid input\n";
        }
        if (userInputInt_ == 1 && northOption != "You can't go North\n") {
            isGoodInput_ = true;
        }
        if (userInputInt_ == 2 && eastOption != "You can't go East\n") {
            isGoodInput_ = true;
        }
        if (userInputInt_ == 3 && southOption != "You can't go South\n") {
            isGoodInput_ = true;
        }
        if (userInputInt_ == 4 && westOption != "You can't go West\n") {
            isGoodInput_ = true;
        }
    }

    //moves the user to the space they specified as long as it isn't a wall
    switch (userInputInt_) {
        case 1: {
            //checks for the escape space
            if (currentSpace_->getNorth() != escapeSpace_) {
                currentSpace_ = currentSpace_->getNorth();
                break;
            }
            //makes sure the user has the key and enough scrolls
            if (currentSpace_->getNorth() == escapeSpace_ && keyCount >= 1 && scrollCount_ >= 5) {
                std::cout << "Would you like to use your key to unlock the door to escape?\n"
                          << "1. Yes\n"
                          << "2. No\n";

                std::cin >> userInputInt_;

                if (userInputInt_ < 1 || userInputInt_ > 2 || std::cin.fail()) {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    std::cout << std::endl;
                    isGoodInput_ = false;
                    std::cout << "Please enter a valid input\n";
                }
                if (userInputInt_ == 1 || userInputInt_ == 2) {
                    isGoodInput_ = true;
                }

                switch (userInputInt_) {
                    case 1: {
                        escapeSpace_->spaceInteraction();
                        isEscaped_ = true;
                        currentSpace_ = currentSpace_->getNorth();
                        break;
                    }

                    case 2: {
                        return;
                    }

                    default: {
                        return;
                    }
                }
            }
            //enforces the key requirement to escape
            if (currentSpace_->getNorth() == escapeSpace_ && keyCount < 1) {
                std::cout << "\n*************************************\n"
                          << "*This door requires a key to unlock.*\n"
                          << "*************************************\n\n";
                break;
            }
            //enforces the 5 scroll requirement to escape
            if (currentSpace_->getNorth() == escapeSpace_ && keyCount >= 1 && scrollCount_ < 5) {
                std::cout << "You need to pickup at least 5 scrolls!\n";
                break;
            }
            break;
        }
        case 2: {
            currentSpace_ = currentSpace_->getEast();
            break;
        }

        case 3: {
            currentSpace_ = currentSpace_->getSouth();
            break;
        }

        case 4: {
            currentSpace_ = currentSpace_->getWest();
            break;
        }
    }
}


//allows the user to pickup items
void game::itemPickup(std::string item) {
    isGoodInput_ = false;
    while (!isGoodInput_) {
        std::cout << "Would you like to pick it up?\n"
                  << "1. Yes\n"
                  << "2. No\n";

        std::cin >> userInputInt_;

        if (userInputInt_ < 1 || userInputInt_ > 2 || std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << std::endl;
            isGoodInput_ = false;
            std::cout << "Please enter a valid input\n";
        }
        if (userInputInt_ == 1 || userInputInt_ == 2) {
            isGoodInput_ = true;
        }
    }

    switch (userInputInt_) {
        case 1: {
            //checks if the bag is full
            if (bagSpot_ < 6) {
                if (item == "Scroll") {
                    scrollCount_++;
                    bagSpot_++;
                }
                if (item == "Key") {
                    keyCount++;
                    bagSpot_++;
                }
                bag_[bagSpot_] = item;
                return;
            }
            if (bagSpot_ >= 6) {
                std::cout << "Your bag is full.\n";
                isGoodInput_ = false;
                while (!isGoodInput_) {
                    std::cout << "Would you like to drop your last picked up item?\n"
                              << "1. Yes\n"
                              << "2. No\n";

                    std::cin >> userInputInt_;

                    if (userInputInt_ < 1 || userInputInt_ > 2 || std::cin.fail()) {
                        std::cin.clear();
                        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                        std::cout << std::endl;
                        isGoodInput_ = false;
                        std::cout << "Please enter a valid input\n";
                    }
                    if (userInputInt_ == 1 || userInputInt_ == 2) {
                        isGoodInput_ = true;
                    }
                }
                switch (userInputInt_) {
                    case 1: {
                        if (bag_[bagSpot_] == "Scroll") {
                            scrollCount_--;
                        }
                        if (bag_[bagSpot_] == "Key") {
                            keyCount--;
                        }
                        if (item == "Scroll") {
                            scrollCount_++;
                        }
                        if (item == "Key") {
                            keyCount++;
                        }
                        bag_[bagSpot_] = item;
                        return;
                    }
                }
            }
        }

        case 2: {
            return;
        }

        default: {
            return;
        }
    }
}

//runs the buildingCollapse function from the fireScrollSpace file, reduces health/increases steps
void game::buildingCollapse() {
    fireDamageEnum temp;
    temp = (currentSpace_->buildingCollapse());
    if (temp == SMOKE) {
        steps_++;
    }
    if (temp == HEALTH) {
        health_--;
    }
}


