/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#ifndef FINALPROJECT_FIRESCROLLSPACE_HPP
#define FINALPROJECT_FIRESCROLLSPACE_HPP

#include "Space.hpp"

class fireScrollSpace : public Space {
public:
    //default constructor
    fireScrollSpace() = default;

    //random event that can cause damage
    fireDamageEnum buildingCollapse() override;

    //event that causes damage to the user
    int fireDamage() override;
};


#endif //FINALPROJECT_FIRESCROLLSPACE_HPP
