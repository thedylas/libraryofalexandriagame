/*********************************************************************
 ** Program name: project3
 ** Author: David Anderson
 ** Date: 02/10/2019
 ** Description: This program is fantasy combat game
 *********************************************************************/

#ifndef DEFAULTFUNCTIONS_MAINMENU_HPP
#define DEFAULTFUNCTIONS_MAINMENU_HPP

#include <iostream>

class menu {
public:
    //constructor
    menu() = default;


    ~menu() = default;


    //primary menu to initiate the game
    void useMainMenu();


    //secondary menu to restart the game once it has been played
    void useSecondaryMenu();


private:
    bool isGoodInput_{};
    int userInputInt_{};
};

#endif //DEFAULTFUNCTIONS_MAINMENU_HPP
