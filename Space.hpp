/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#ifndef FINALPROJECT_SPACE_HPP
#define FINALPROJECT_SPACE_HPP

#include <iostream>

enum spaceEnum {
    START = 1, ENTRANCE = 2, STAIRS = 3, WESTGARDEN = 4, EASTGARDEN = 5,
    SOUTHEASTHALLWAY = 6, SOUTHMAINLIBRARY = 7, SOUTHWESTHALLWAY = 8, LIBRARYWESTHALL = 9, LIBRARYMAINHALL = 10,
    LIBRARYEASTHALL = 11, NORTHEASTHALL = 12, NORTHMAINLIBRARY = 13, NORTHWESTHALL = 14, ESCAPE = 15
};

enum fireDamageEnum {
    NONE = 0, HEALTH = 1, SMOKE = 2
};

class Space {
public:
    //default constructor
    Space() = default;


    //destructor
    virtual ~Space();


    //allows the user to interact with the space
    virtual void spaceInteraction();


    //deals damage to the user
    virtual int fireDamage();


    //can deal damage to the user
    virtual fireDamageEnum buildingCollapse();


    //getters
    Space *getNorth();


    Space *getEast();


    Space *getSouth();


    Space *getWest();


    std::string getSpaceName();


    spaceEnum getSpaceEnum();


    //setters
    void setNorth(Space *);


    void setEast(Space *);


    void setSouth(Space *);


    void setWest(Space *);


    void setSpaceName(std::string);


    void setIsDiscovered(bool);


    void setSpaceEnum(spaceEnum);


    void setEscapeLocked(bool);


private:
    Space *north_{};
    Space *east_{};
    Space *south_{};
    Space *west_{};
    std::string spaceName_ = "";
    spaceEnum spaceEnumName_;
};


#endif //FINALPROJECT_SPACE_HPP