/*********************************************************************
 ** Program name: Final Project
 ** Author: David Anderson
 ** Date: 03/10/2019
 ** Description: This program is a game centered around the Library of Alexandria.
 ** The library is burning and you must collect important scrolls before  it burns down.
 *********************************************************************/

#ifndef FINALPROJECT_GAME_HPP
#define FINALPROJECT_GAME_HPP

#include "fireScrollSpace.hpp"
#include "keyFireScrollSpace.hpp"
#include "neutralSpace.hpp"
#include "scrollSpace.hpp"
#include "lockedSpace.hpp"
#include "Space.hpp"

class game {
public:
    game() = default;


    ~game() {
        delete startSpace_;
        delete entranceSpace_;
        delete stairsSpace_;
        delete westGardenSpace_;
        delete eastGardenSpace_;
        delete southMainLibrarySpace_;
        delete southWestHallSpace_;
        delete southEastHallSpace_;
        delete libraryWestHallSpace_;
        delete libraryEastHallSpace_;
        delete libraryMainHallSpace_;
        delete northWestHallSpace_;
        delete northEastHallSpace_;
        delete northMainLibrarySpace_;
        delete escapeSpace_;
        delete wallSpace_;
    };


    void createGame();


    void playGame();


    void turn();


    void narrative();


    void printMap();


    void directionDecision();


    void itemPickup(std::string);


    void buildingCollapse();


private:
    Space *currentSpace_{};

    Space *startSpace_ = new neutralSpace;
    Space *entranceSpace_ = new neutralSpace;
    Space *stairsSpace_ = new scrollSpace;
    Space *westGardenSpace_ = new neutralSpace;
    Space *eastGardenSpace_ = new neutralSpace;
    Space *southMainLibrarySpace_ = new fireScrollSpace;
    Space *southWestHallSpace_ = new fireScrollSpace;
    Space *southEastHallSpace_ = new scrollSpace;
    Space *libraryWestHallSpace_ = new keyFireScrollSpace;
    Space *libraryEastHallSpace_ = new keyFireScrollSpace;
    Space *libraryMainHallSpace_ = new fireScrollSpace;
    Space *northWestHallSpace_ = new fireScrollSpace;
    Space *northEastHallSpace_ = new fireScrollSpace;
    Space *northMainLibrarySpace_ = new fireScrollSpace;
    Space *escapeSpace_ = new lockedSpace;
    Space *wallSpace_ = new neutralSpace;

    int health_ = 9;
    int steps_ = 0;

    bool isEscaped_ = false;

    int userInputInt_ = 0;
    bool isGoodInput_ = false;

    int keyCount = 0;
    int scrollCount_ = 0;
    int bagSpot_ = -1;
    std::string bag_[7];
};


#endif //FINALPROJECT_GAME_HPP
